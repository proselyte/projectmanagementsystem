package net.proselyte.pmsystem.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Simple JavaBean domain object that represents a Skill (Java, SQL, Spring, etc.)
 *
 * @author Eugene Suleimanov
 */

@Entity
@Table(name = "skills")
public class Skill extends NamedEntity {

    public Skill() {

    }

    public Skill(Long id, String data) {
        super(data);
        this.setId(id);
    }
}
