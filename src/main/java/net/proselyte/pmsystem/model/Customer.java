package net.proselyte.pmsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents a Customer (AvalBank, McDonalds, etc.)
 *
 * @author Eugene Suleimanov
 */

@Entity
@Table(name = "customers")
public class Customer extends NamedEntity {

    @Column(name = "description")
    private String description;

    public Customer(){

    }

    public Customer(String description) {
        this.description = description;

    }

    public Customer(Long id, String name, String description) {
        super(name);
        this.setId(id);
        this.description = description;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "id='" + getId()+ '\'' +
                "name='" + getName() + '\'' +
                "description='" + description + '\'' +
                '}';
    }
}
