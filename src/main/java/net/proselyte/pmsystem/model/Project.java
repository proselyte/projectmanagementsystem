package net.proselyte.pmsystem.model;


import javax.persistence.*;

/**
 * Simple JavaBean domain object that represents a Project.
 *
 * @author Eugene Suleimanov
 * @author Kyryl Potapenko
 */
@Entity
@Table(name = "projects")
public class Project extends NamedEntity {

    @Column(name = "description")
    private String description;

    //@Column(name = "companyId")

    @PrimaryKeyJoinColumn(name = "companies", referencedColumnName = "id")
    private Long companyId;

    //@Column(name = "documentId")

    @PrimaryKeyJoinColumn(name = "documents", referencedColumnName = "id")
    private Long documentId;

    //@Column(name = "teamId")

    @PrimaryKeyJoinColumn(name = "teams", referencedColumnName = "id")
    private Long teamId;

    public void setCompanyId(Long CompanyId) {
        this.companyId = CompanyId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Project(String name, String description) {
        super(name);
        this.description = description;
    }

    public Project() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCompanyId() {
        return companyId;
    }


    public Long getDocumentId() {
        return documentId;
    }


    public Long getTeamId() {
        return teamId;
    }


    @Override
    public String toString() {
        return "Project{" +
                "Id='" + getId() + '\'' +
                ", name='" + getName() + '\'' +
                ", description='" + description + '\'' +
                ", companyId=" + companyId +
                ", documentId=" + documentId +
                ", teamId=" + teamId +
                '}';
    }
}
