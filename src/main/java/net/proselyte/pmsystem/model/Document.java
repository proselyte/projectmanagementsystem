package net.proselyte.pmsystem.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumns;
import javax.persistence.Table;

/**
 * Simple JavaBean domain object that represents a Document (for instance, Project documentation)
 *
 * @author Eugene Suleimanov
 */

@Entity
@Table(name = "documents")
public class Document extends NamedEntity {


    public Document() {
    }

    @Column(name = "content")
    private String content;

    public Document(String content) {
        this.content = content;
    }

    public Document(Long id, String name, String content) {

        super(name);
        this.setId(id);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Document{" +
                "Id='" + getId() + '\'' +
                ", name='" + getName() + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
