package net.proselyte.pmsystem.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Simple JavaBean domain object that represents a Speciality of {@link Developer} (Java Developer, C++ Developer, etc.)
 *
 * @author Eugene Suleimanov
 */

@Entity
@Table(name = "specialities")
public class Speciality extends NamedEntity {

}
