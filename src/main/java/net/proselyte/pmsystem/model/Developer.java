package net.proselyte.pmsystem.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Simple JavaBean domain object that represents a Developer.
 *
 * @author Eugene Suleimanov
 *  Updated by Iryna Seliutina on 14.01.2016.
 */

@Entity
@Table(name = "developers")
public class Developer extends NamedEntity {

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "age")
    private Integer age;

    @Column(name = "salary")
    private BigDecimal salary;

    @Column(name = "yearsOfExperience")
    private Integer yearsOfExperience;

    @Column(name = "experience")
    private String experience;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "developer_skills",
            joinColumns = @JoinColumn(name = "developersId"),
            inverseJoinColumns = @JoinColumn(name = "skillsId")
    )
    private Set<Skill> skills;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "developer_specialities",
            joinColumns = @JoinColumn(name = "developersId"),
            inverseJoinColumns = @JoinColumn(name = "specialitiesId")
    )
    private Set<Speciality> specialties;

    public Developer() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Integer getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(Integer yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Set<Speciality> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(Set<Speciality> specialties) {
        this.specialties = specialties;
    }

    @Override
    public String toString() {
        return "Developer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", yearsOfExperience=" + yearsOfExperience +
                ", experience='" + experience + '\''  +
                '}';
    }
}