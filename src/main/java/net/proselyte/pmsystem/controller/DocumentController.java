package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.hibernate.HibernateDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;
import net.proselyte.pmsystem.util.HibernateUtil;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.hibernate.cfg.Configuration;
import org.jboss.logging.Logger;

import java.util.Collection;

/**
 * Created by Orange on 17.12.2016.
 */
public class DocumentController implements Controller<Document, Long> {

    //private static final Logger LOGGER = LoggerFactory.logger(DocumentController.class);

    HibernateDocumentDAOImpl hbtDocumentDAO = new HibernateDocumentDAOImpl(new Configuration().configure().buildSessionFactory());


    @Override
    public Document getById(Long aLong) {
        return hbtDocumentDAO.getById(aLong);
    }

    @Override
    public void save(Document document) {
        hbtDocumentDAO.save(document);
    }

    @Override
    public void update(Document document) {
        hbtDocumentDAO.update(document);
    }

    @Override
    public void remove(Document document) {
        hbtDocumentDAO.remove(document);
    }

    public Collection<Document> showAllBase() {
        Collection<Document> documents;
        documents = hbtDocumentDAO.showAllBase();
        return documents;
    }
}

