package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.hibernate.HibernateSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;
import net.proselyte.pmsystem.util.HibernateUtil;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Raketa on 23.01.2017.
 */
public class HibernateSpecialityController implements Controller<Speciality, Long> {


  HibernateSpecialityDAOImpl hibernateSpecialityDAO = new HibernateSpecialityDAOImpl(HibernateUtil.getSessionFactory());

  @Override
  public Speciality getById(Long id) {
    return hibernateSpecialityDAO.getById(id);
  }

  @Override
  public void save(Speciality speciality) {
    hibernateSpecialityDAO.save(speciality);
  }

  @Override
  public void update(Speciality speciality) {
    hibernateSpecialityDAO.update(speciality);
  }

  @Override
  public void remove(Speciality speciality) {
    hibernateSpecialityDAO.remove(speciality);
  }

  public Collection<Speciality> getAll() {
    Collection<Speciality> list = new ArrayList<>();
    return list = hibernateSpecialityDAO.getAll();
  }
}
