package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.hibernate.HibernateDeveloperDaoImpl;
import net.proselyte.pmsystem.model.Developer;
import net.proselyte.pmsystem.model.Skill;
import net.proselyte.pmsystem.model.Speciality;
import org.hibernate.cfg.Configuration;

import java.util.Set;

/**
 * Created by Orange on 25.12.2016.
 *  Updated by Iryna Seliutina on 14.01.2016.
 *  Updated by Kyryl Potapenko on 15.02.2017.
 */
public class DeveloperController implements Controller<Developer, Long> {
    HibernateDeveloperDaoImpl hibernateDeveloperDao = new HibernateDeveloperDaoImpl(new Configuration().configure().buildSessionFactory());

    public Developer getById(Long id){
        return hibernateDeveloperDao.getById(id);
    }

    public void save (Developer developer) {
        hibernateDeveloperDao.save(developer);
    }

    public  void  update (Developer developer){
        hibernateDeveloperDao.update(developer);
    }

    public void remove (Developer developer){
        hibernateDeveloperDao.remove(developer);
    }

    public void showAllDevelopers (Developer developer) {
        hibernateDeveloperDao.showAllDevelopers(developer);
    }

        public  void addSkillToDeveloperList (Skill skill){
            Developer developer = new Developer();
            Set<Skill> skills = developer.getSkills();
            skills.add(skill);
            developer.setSkills(skills);
        }

        public void addSpecialityToDeveloperList (Speciality speciality){
        Developer developer = new Developer();
        Set<Speciality> specialities = developer.getSpecialties();
        specialities.add(speciality);
        developer.setSpecialties(specialities);
        }
}
