package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.hibernate.HibernateSpecialityDAOImpl;
import net.proselyte.pmsystem.model.Speciality;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Raketa on 23.01.2017.
 */
public class SpecialityController implements Controller<Speciality, Long> {


  HibernateSpecialityDAOImpl hibernateSpecialityDAO = new HibernateSpecialityDAOImpl(new Configuration().configure().buildSessionFactory());

  @Override
  public Speciality getById(Long id) {
    return hibernateSpecialityDAO.getById(id);
  }

  @Override
  public void save(Speciality speciality) {
    hibernateSpecialityDAO.save(speciality);
  }

  @Override
  public void update(Speciality speciality) {
    hibernateSpecialityDAO.update(speciality);
  }

  @Override
  public void remove(Speciality speciality) {
    hibernateSpecialityDAO.remove(speciality);
  }

  public Collection<Speciality> getAll() {
    Collection<Speciality> list = new ArrayList<>();
    return list = hibernateSpecialityDAO.getAll();
  }
}