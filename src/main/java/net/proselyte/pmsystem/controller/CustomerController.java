package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.controller.Controller;
import net.proselyte.pmsystem.dao.hibernate.HibernateCustomerDAOImpl;
import net.proselyte.pmsystem.model.Customer;
import net.proselyte.pmsystem.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Руслан on 18.01.2017.
 * Edited by Iryna 14.02.2017.
 */
public class CustomerController implements Controller<Customer, Long> {
    HibernateCustomerDAOImpl hibernateCustomerDAOCustomerDAO = new HibernateCustomerDAOImpl(HibernateUtil.getSessionFactory());

    public Customer getById(Long customerId) {
        return hibernateCustomerDAOCustomerDAO.getById(customerId);
    }

    public void save(Customer customer) {
        hibernateCustomerDAOCustomerDAO.save(customer);
    }

    public void update(Customer customer) {
        hibernateCustomerDAOCustomerDAO.update(customer);
    }

    public void remove(Customer customer) {
        hibernateCustomerDAOCustomerDAO.remove(customer);
    }

    public List<Customer> showAll() {
        ArrayList<Customer> list;
        list = (ArrayList<Customer>) hibernateCustomerDAOCustomerDAO.showAll();
        return list;
    }

}
