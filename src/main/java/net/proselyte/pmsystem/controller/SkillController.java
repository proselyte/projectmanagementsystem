package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.hibernate.HibernateSkillDAOImpl;
import net.proselyte.pmsystem.model.Skill;
import net.proselyte.pmsystem.util.HibernateUtil;

import java.util.Collection;

/**
 * Controller that handles requests connected with {@link Skill}
 *
 * @author Irina Selutina
 * @author Kyryl Potapenko
 */
public class SkillController implements Controller<Skill, Long> {

    HibernateSkillDAOImpl hibernateSkillDAO = new HibernateSkillDAOImpl(HibernateUtil.getSessionFactory());

    public Skill getById(Long id) {
        return hibernateSkillDAO.getById(id);
    }

    public void save(Skill skill) {
        hibernateSkillDAO.save(skill);
    }

    public void update(Skill skill) {
        hibernateSkillDAO.update(skill);
    }

    public void remove(Skill skill) {
        hibernateSkillDAO.remove(skill);
    }

    public Collection<Skill> showAllBase() {
        Collection<Skill> skills;
        skills = hibernateSkillDAO.showAllBase();
        return skills;
    }
}