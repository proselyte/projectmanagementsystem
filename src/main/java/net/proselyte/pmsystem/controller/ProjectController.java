package net.proselyte.pmsystem.controller;

import net.proselyte.pmsystem.dao.hibernate.HibernateProjectDAOImpl;
import net.proselyte.pmsystem.model.Project;
import org.hibernate.cfg.Configuration;

import java.io.File;
import java.util.List;

/**
 * Controller that handles requests from View connected with {@link Project}
 *
 * @author Kyryl Potapenko
 */
public class ProjectController implements Controller<Project, Long> {
    HibernateProjectDAOImpl hibernateProjectDAO = new HibernateProjectDAOImpl(new Configuration().configure(new File("D:/GO_IT4/projectmanagementsystem/src/main/resources/hibernate.cfg.xml")).buildSessionFactory());

    @Override
    public Project getById(Long aLong) {
        return hibernateProjectDAO.getById(aLong);
    }

    @Override
    public void save(Project entity) {
        hibernateProjectDAO.save(entity);
    }

    @Override
    public void update(Project entity) {
        hibernateProjectDAO.update(entity);

    }

    @Override
    public void remove(Project entity) {
        hibernateProjectDAO.remove(entity);

    }

    public List<Project> showAllprojects() {
        return hibernateProjectDAO.getAll();
    }

}
