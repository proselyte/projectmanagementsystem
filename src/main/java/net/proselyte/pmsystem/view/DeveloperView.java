package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.DeveloperController;
import net.proselyte.pmsystem.model.Developer;

import java.io.IOException;
import java.math.BigDecimal;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Class that contains method to output data connected with {@link Developer}
 *
 * @author Kyryl Potapenko/
 * Updated by Iryna Seliutina on 14.01.2016.
 */
public class DeveloperView {
    public void execute() throws IOException {
        Developer developer = new Developer();
        DeveloperController developerController = new DeveloperController();
        String data;
        Long id;
        Integer integer;
        BigDecimal salary;

        writeMessage("* * * Разработчики * * *" + "\n" +
                "1 - Add \n"+ "2 - Delete \n"+ "3 - Update \n"+ "4 - get by ID \n"+ "5 - Show all developers \n"+ "6 - Exit to the main menu \n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developer.setId(id);
                writeMessage("Enter developer name:\n");
                data = readString();
                developer.setFirstName(data);
                writeMessage("\nEnter developer surname:\n");
                data = readString();
                developer.setLastName(data);
                writeMessage("\nEnter developer age:\n");
                integer = readInt();
                developer.setAge(integer);
                writeMessage("\nEnter developer salary:\n");
                salary = readBigDecimal();
                developer.setSalary(salary);
                writeMessage("\nENter developer experience:\n");
                integer = readInt();
                developer.setYearsOfExperience(integer);
                writeMessage("Enter the developer experience:\n");
                data = readString();
                developer.setExperience(data);
                writeMessage("\nDeveloper created!\n");
                developerController.save(developer);
                System.out.println(developerController.getById(id).toString());
                break;
            case 2:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developerController.remove(developerController.getById(id));
                writeMessage("\nDeveloper remove!\n");
                break;
            case 3:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developer.setId(id);
                writeMessage("Enter new name:\n");
                data = readString();
                developer.setFirstName(data);
                writeMessage("\nEnter new surname:\n");
                data = readString();
                developer.setLastName(data);
                writeMessage("\nEnter developer age:\n");
                integer = readInt();
                developer.setAge(integer);
                writeMessage("\nEnter new salary:\n");
                salary = readBigDecimal();
                developer.setSalary(salary);
                writeMessage("\nEnter experience:\n");
                integer = readInt();
                developer.setYearsOfExperience(integer);
                writeMessage("Select Developer specialization:\n");
                data = readString();
                developer.setExperience(data);
                developerController.update(developer);
                writeMessage("\nUpdate complite!\n");
                break;
            case 4:
                writeMessage("Enter developer ID:\n");
                id = readLong();
                developerController.getById(id);
                System.out.println(developerController.getById(id).toString());
                break;
            case 5:
                developerController.showAllDevelopers(developer);
                break;
            case 6:
                writeMessage("Exit to the main menu:\n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
            default:
                break;
        }
        execute();
    }
}