package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.CompanyController;
import net.proselyte.pmsystem.model.Company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Class that contains method to output data connected with {@link Company}
 *
 * @author Kyryl Potapenko
 * @author of updated version Oleksii Samantsov
 */
public class CompanyView {

    public void execute() throws IOException {
        Company Company = new Company();
        CompanyController CompanyController = new CompanyController();
        String name;
        Long id;
        String description;

        writeMessage("* * * Companies * * *" + "\n" +
                " 1 - Add \n" +
                " 2 - Remove \n" +
                " 3 - Update \n" +
                " 4 - Search by ID \n" +
                " 5 - Show the company's customers \n" +
                " 6 - Show all companies \n" +
                " 7 - Return to main menu\n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                this.add();
                break;
            case 2:
                this.remove();
                break;
            case 3:
                this.update();
                break;
            case 4:
                this.getAndPrintCompany();
                break;
            case 5:
                this.showCustomers();
                break;
            case 6:
                this.showAll();
                break;
            case 7:
                this.exit();
                break;
            default:
                break;
        }
        execute();
    }

    private void showCustomers() throws IOException {
        Long id = null;
        try {
            id = getNumber();
            while (!checkIfExist(id)) {
                writeMessage("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        CompanyController companyController = new CompanyController();
        writeMessage("Clients of company:");
        companyController.showRelatedcustomers(id);
        writeMessage("\nProjects of company:");
        companyController.showRelatedprojectsAndDev(id);
    }

    private void showAll() {
        CompanyController companyController = new CompanyController();
        writeMessage("Show list of all companies: \n");
        Collection<Company> list = new ArrayList<Company>();
        list = companyController.getAll();
        for (Company Company1 : list) {
            writeMessage(Company1.getId() + ", " + Company1.getName() + ", " + Company1.getDescription() + "\n");
        }
    }

    private void getAndPrintCompany() throws IOException {
        Company company = null;
        try {
            company = getCompany();
        } catch (Exception e) {
            e.printStackTrace();
        }
        writeMessage(company.toString() + "\n");

    }

    private void update() throws IOException {
        Long id = null;
        String name = null;
        String description = null;

        try {
            id = getNumber();
            while (!checkIfExist(id)) {
                writeMessage("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        writeMessage("Enter new company's name:\n");
        try {
            name = readString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeMessage("Enter new company's description:\n");
        try {
            description = readString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Company company = new Company(id, name, description);
        CompanyController companyController = new CompanyController();
        companyController.update(company);
        writeMessage("\nThe changes implemented!\n");
    }

    private void remove() throws IOException {
        Company company = null;
        try {
            company = getCompany();
        } catch (Exception e) {
            e.printStackTrace();
        }
        CompanyController companyController = new CompanyController();
        companyController.remove(company);
        writeMessage("\nThe company removed!\n");
    }

    private Company getCompany() {
        Long id = null;
        try {
            id = getNumber();
            while (!checkIfExist(id)) {
                writeMessage("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Company company;
        CompanyController companyController = new CompanyController();
        return company = companyController.getById(id);
    }

    private void add() throws IOException {
        Company company = new Company();
        CompanyController companyController = new CompanyController();
        Long id = null;
        String name = null;
        String description = null;
        try {
            id = getNumber();
            while (checkIfExist(id)) {
                writeMessage("Try another number");
                id = getNumber();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        writeMessage("Enter company's name:\n");
        try {
            name = readString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        writeMessage("\nEnter description of the company:");
        try {
            description = readString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeMessage("\nData entered!\n");
        company.setId(id);
        company.setName(name);
        company.setDescription(description);
        companyController.save(company);
        System.out.println(companyController.getById(id).toString());
    }

    private boolean checkIfExist(Long id) {
        boolean result = false;
        CompanyController companyController = new CompanyController();
        Company company = new Company();
        try {
            company = companyController.getById(id);
            if (company != null) {
                result = true;
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    private void exit() throws IOException {
        writeMessage("We're about to main menu...");
        ConsoleHelper consoleHelper = new ConsoleHelper();
        consoleHelper.consoleHelp();
    }

    private Long getNumber() {
        Scanner scanner = new Scanner(System.in);
        writeMessage("Enter ID of the company");
        while (!scanner.hasNextLong()) {
            writeMessage("Invalid input\n Type the Long-type number:");
            scanner.next();
        }
        Long number = scanner.nextLong();
        return number;
    }


}