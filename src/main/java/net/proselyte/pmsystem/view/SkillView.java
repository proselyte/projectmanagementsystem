package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.SkillController;
import net.proselyte.pmsystem.model.Skill;

import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * @author Created by Iryna on 26.12.16.
 * @author Kyryl Potapenko
 */
public class SkillView {

    String data;
    Long id;

    public SkillView() {
    }

    public void skillView() throws IOException {
        writeMessage("* * * Skills * * *" + "\n" +
                " 1 - Add \n" +
                " 2 - Remove \n" +
                " 3 - Alter \n" +
                " 4 - Find by id \n" +
                " 5 - Show all records \n" +
                " 6 - Exit in main menu\n");
        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                this.add();
                break;
            case 2:
                this.remove();
                break;
            case 3:
                this.update();
                break;
            case 4:
                this.getById();
                break;
            case 5:
                this.getAllSkills();
                break;
            case 6:
                this.exit();
                break;
            default:
                break;
        }
        skillView();
    }

    private void exit() {
        writeMessage("We're about to main menu...");
        ConsoleHelper consoleHelper = new ConsoleHelper();
        try {
            consoleHelper.consoleHelp();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getAllSkills() {
        SkillController skillController = new SkillController();
        Collection<Skill> skills;
        skills = skillController.showAllBase();
        for (Skill skill : skills) {
            writeMessage(skill.toString() + "\n");
        }

    }

    private void getById() throws IOException {

        Skill skill = this.getSkill();

        writeMessage(skill.toString());
    }

    private void update() throws IOException {
        Long id = null;
        String data = null;

        try {
            id = getNumber();
            while (!checkIfExist(id)) {
                writeMessage("Try another number\n");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        writeMessage("Enter new Skill name:\n");
        try {
            data = readString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Skill skill = new Skill(id, data);
        SkillController skillController = new SkillController();
        skillController.update(skill);
        writeMessage("\nThe changes implemented!\n");
    }

    private void remove() throws IOException {
        Skill skill = null;
        try {
            skill = getSkill();
        } catch (Exception e) {
            e.printStackTrace();
        }
        SkillController skillController = new SkillController();
        skillController.remove(skill);
        writeMessage("\nThe skill removed!\n");
    }

    private Skill getSkill() {
        Long id = null;
        try {
            id = getNumber();
            while (!checkIfExist(id)) {
                writeMessage("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Skill skill;
        SkillController skillController = new SkillController();
        return skill = skillController.getById(id);
    }

    private void add() throws IOException {
        Long id = null;
        String data = null;

        try {
            id = getNumber();
            while (checkIfExist(id)) {
                writeMessage("Try another number");
                id = getNumber();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        writeMessage("Enter name of skill:\n");
        try {
            data = readString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Skill skill = new Skill(id, data);

        SkillController skillController = new SkillController();
        skillController.save(skill);
        writeMessage("\nData entered!\n");
    }

    private boolean checkIfExist(Long id) {
        boolean result = false;
        SkillController skillController = new SkillController();
        Skill skill;
        try {
            skill = skillController.getById(id);
            if (skill != null) {
                result = true;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    private Long getNumber() {
        Scanner scanner = new Scanner(System.in);
        writeMessage("Enter ID of the skill");
        while (!scanner.hasNextLong()) {
            writeMessage("Invalid input\n Type the Long-type number:");
            scanner.next();
        }
        Long number = scanner.nextLong();
        return number;
    }




}