package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.DocumentController;
import net.proselyte.pmsystem.dao.hibernate.HibernateDocumentDAOImpl;
import net.proselyte.pmsystem.dao.jdbc.JdbcDocumentDAOImpl;
import net.proselyte.pmsystem.model.Document;

import javax.print.Doc;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Scanner;

import static net.proselyte.pmsystem.view.ConsoleHelper.readLong;
import static net.proselyte.pmsystem.view.ConsoleHelper.readString;
import static net.proselyte.pmsystem.view.ConsoleHelper.writeMessage;

/**
 * Created by proselyte on 21.12.16.
 */
public class DocumentView {
    private Scanner input = new Scanner(System.in);
    Document document = new Document();
    DocumentController documentController = new DocumentController();
    Long id;
    String name;
    String content;

    public void showMainMenu() throws IOException {
        System.out.println("    Document Menu ");
        System.out.println("1. Add document");
        System.out.println("2. Get document by ID");
        System.out.println("3. Delete document");
        System.out.println("4. Edit document");
        System.out.println("5. Show to all document");
        System.out.println("6. Return to Main Menu");

        int selection = input.nextInt();
        input.nextLine();

        switch (selection) {
            case 1:
                writeMessage("Add document ID");
                id = readLong();
                writeMessage("Add document name");
                name = readString();
                writeMessage("Add document context");
                content = readString();
                document.setId(id);
                document.setName(name);
                document.setContent(content);
                documentController.save(document);
                System.out.println("Document add!!!");
                showMainMenu();
                break;
            case 2:
                this.getAndPrintDocument();
                break;
            case 3:
                this.delete();
                break;
            case 4:
                writeMessage("Add document ID");
                id = readLong();
                writeMessage("Add document name");
                name = readString();
                writeMessage("Add document context");
                content = readString();
                document.setId(id);
                document.setName(name);
                document.setContent(content);
                documentController.update(document);
                System.out.println("Document update!!!");
                showMainMenu();
                break;
            case 5:
                System.out.println("List document \n");
                this.getAllDocuments();
            case 6:
                this.exit();
                break;
            default:
                System.out.println("Invalid selection");
        }
    }

    private void getAndPrintDocument() {
        Document document = null;
        try {
            document = getDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(document.toString());
        System.out.println();
        System.out.println("************************");
        try {
            showMainMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void exit() {
        System.out.println("We're about to main menu...");
        ConsoleHelper consoleHelper = new ConsoleHelper();
        try {
            consoleHelper.consoleHelp();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void delete() {
        Document document = null;
        try {
            document = getDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DocumentController documentController = new DocumentController();
        documentController.remove(document);
        System.out.println("Data successfully deleted");
        try {
            showMainMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Document getDocument() {
        Long id = null;
        try {
            id = getNumber();
            while (!checkIfExist(id)) {
                System.out.println("Try another number");
                id = getNumber();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        DocumentController documentController = new DocumentController();
        return document = documentController.getById(id);
    }

    private Long getNumber() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter ID of the document");
        while (!scanner.hasNextLong()) {
            System.out.println("Invalid input\n Type the Long-type number:");
            scanner.next();
        }
        Long number = scanner.nextLong();

        return number;
    }

    private boolean checkIfExist(Long aLong) {
        boolean result = false;
        DocumentController documentController = new DocumentController();
        Document document;
        try {
            document = documentController.getById(aLong);
            if (document != null) {
                result = true;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    private void getAllDocuments() {
        DocumentController documentController = new DocumentController();
        Collection<Document> documentCollection;
        documentCollection = documentController.showAllBase();
        for (Document document : documentCollection) {
            writeMessage(document.toString() + "\n");
        }
    }
}