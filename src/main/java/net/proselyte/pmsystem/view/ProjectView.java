package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.ProjectController;
import net.proselyte.pmsystem.model.Project;

import java.io.IOException;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Class that contains method to output data connected with {@link Project}
 *
 * @author Kyryl Potapenko
 */
public class ProjectView {
    ProjectController projectController;
    Long id;
    String nameProj;
    String descrProj;
    Long idTeam;
    Long idDoc;
    Long idCompany;


    public ProjectView() {
        projectController = new ProjectController();
    }

    public void projectView() throws IOException {
        Project project;
        writeMessage("\n * * * Project * * *" + "\n" +
                "1 - Add project | 2 - Remove project | 3 - Edit project | 4 - Search by ID  | 5 - Show all project  | 6 - Exit to menu");

        int commandUser = readInt();

        switch (commandUser) {
            case 1:

                writeMessage("Note that the fields ID team,document and Company must match those already have. \n Enter please Id number of project: \n");
                project = new Project();
                id = readLong();
                project.setId(id);
                writeMessage("Enter please name project: \n ");
                nameProj = readString();
                project.setName(nameProj);
                writeMessage("Enter please description project: \n");
                descrProj = readString();
                project.setDescription(descrProj);
                writeMessage("Please enter id of team: \n");
                idTeam = readLong();
                project.setTeamId(idTeam);
                writeMessage("Please enter id of document: \n");
                idDoc = readLong();
                project.setDocumentId(idDoc);
                writeMessage("Please enter id of Company: \n");
                idCompany = readLong();
                project.setCompanyId(idCompany);
                projectController.save(project);
                projectView();
                break;
            case 2:
                writeMessage("Enter please ID project to delete: \n");
                project = new Project();
                id = readLong();
                project.setId(id);
                projectController.remove(project);
                projectView();
                break;
            case 3:
                writeMessage("Note that the fields ID team,document and Company must match those already have. \n Enter please name project: \n");
                project = new Project();
                nameProj = readString();
                project.setName(nameProj);
                writeMessage("enter  description: \n");
                descrProj = readString();
                project.setDescription(descrProj);
                writeMessage("Please enter id of team: \n");
                idTeam = readLong();
                project.setTeamId(idTeam);
                writeMessage("Please enter id of document: \n");
                idDoc = readLong();
                project.setDocumentId(idDoc);
                writeMessage("Please enter id of Company: \n");
                idCompany = readLong();
                project.setCompanyId(idCompany);
                writeMessage("enter ID project: \n");
                id = readLong();
                project.setId(id);
                projectController.update(project);
                projectView();
                break;
            case 4:
                writeMessage("Enter plese ID project: \n");
                id = readLong();
                projectController.getById(id);
                projectView();
                break;
            case 5:
                writeMessage("All projects:\n");
                projectController.showAllprojects();
                projectView();
                break;
            case 6:
                writeMessage("Exiting to main menu \n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;

            default:
                break;

        }

    }

}

