package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.TeamController;
import net.proselyte.pmsystem.model.Team;

import java.io.IOException;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Class that contains method to output data connected with {@link Team}.
 */
public class TeamView {
    public void choice() throws IOException {
        Team team = new Team();
        Long id;
        String name;

        TeamController teamController = new TeamController();

        writeMessage("\n 1 - Add team \n 2 - Delete team \n 3 - Change team \n 4 - Find team by ID \n 5 - Show all teams \n 6 - Exit to main menu\n");

        int commandNumber = readInt();

        switch (commandNumber) {
            case 1:
                writeMessage("\n Enter team name");
                name = readString();
                writeMessage("\n Enter team Id");
                id = readLong();
                writeMessage("\n Team created");

                team.setId(id);
                team.setName(name);
                teamController.save(team);
                choice();
                break;

            case 2:
                writeMessage("\n Enter team Id");
                id = readLong();
                teamController.remove(teamController.getById(id));
                writeMessage("\n Team deleted \n");
                choice();
                break;

            case 3:
                writeMessage("\n Enter team Id");
                id = readLong();
                writeMessage("\n Enter new team name");
                name = readString();

                team.setId(id);
                team.setName(name);

                teamController.update(team);
                System.out.println("\n Team changed \n");
                choice();
                break;

            case 4:
                writeMessage("\n Enter team Id");
                id = readLong();
                teamController.getById(id);
                System.out.println(teamController.getById(id).toString());
                choice();
                break;

            case 5:
                writeMessage("All teams:");
                teamController.showAllTeams(team);
                choice();
                break;

            case 6:
                writeMessage("Exiting to main menu...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;

            default:
                break;

        }
        choice();
    }
}
