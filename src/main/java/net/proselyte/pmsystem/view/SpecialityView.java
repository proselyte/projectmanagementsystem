package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.SpecialityController;
import net.proselyte.pmsystem.model.Speciality;

import java.io.IOException;
import java.util.Collection;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Created by Raketa on 01.01.2017.
 */
public class SpecialityView {

    public void specialityView() throws IOException {
        Speciality speciality = new Speciality();
        SpecialityController specialityController = new SpecialityController();
        Long id;
        String name;

        writeMessage("" +
                "0 - Display all specialty\n" +
                "1 - Add specialty \n" +
                "2 - Remove specialty \n" +
                "3 - Update specialty \n" +
                "4 - Get  specialty by ID \n" +
                "5 - Exit to the main menu\n");


        int choice = readInt();

        switch (choice) {
            case 0:
                Collection<Speciality> specialities = specialityController.getAll();
                specialities.forEach(System.out::println);
                break;
            case 1:
                writeMessage("Enter specialty name \n");
                name = readString();
                writeMessage("Enter specialty ID\n");
                id = readLong();
                writeMessage("\n Specialty created \n");

                speciality.setId(id);
                speciality.setName(name);

                specialityController.save(speciality);
                break;

            case 2:
                writeMessage("\n Entwr specialty ID\n");
                id = readLong();
                specialityController.remove(specialityController.getById(id));
                writeMessage("\n Rpecialty remove \n");
                break;

            case 3:
                writeMessage("\n Enter specialty ID \n");
                id = readLong();
                writeMessage("\n Enter new specialty name\n");
                name = readString();

                speciality.setId(id);
                speciality.setName(name);

                specialityController.update(speciality);
                System.out.printf("\n Data successfully changed \n");
                break;

            case 4:
                writeMessage("\n Enter specialty id \n");
                id = readLong();
                specialityController.getById(id);
                System.out.println(specialityController.getById(id).toString());
                break;
            case 5:
                writeMessage("\n Exit to the main menu...\n");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }
        specialityView();
    }
}