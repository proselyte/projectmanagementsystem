package net.proselyte.pmsystem.view;

import net.proselyte.pmsystem.controller.CustomerController;
import net.proselyte.pmsystem.model.Customer;

import java.io.IOException;

import static net.proselyte.pmsystem.view.ConsoleHelper.*;

/**
 * Created by proselyte on 21.12.16.
 */
public class CustomerView {

    public void customerView() throws IOException {
        Customer customer = new Customer();
        CustomerController customerController = new CustomerController();
        Long id;
        String name;
        String description;

        writeMessage(" 1 - Add customer \n 2 - Remove customer \n 3 - Edit customer \n 4 - Search customer by ID \n 5 - Show all customer \n 6 - Exit on main menu \n");

        int choice = readInt();

        switch (choice) {
            case 1:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                writeMessage("Enter customer name \n");
                name = readString();
                writeMessage("\n Enter description \n");
                description = readString();
                writeMessage("\n Done \n");

                customer.setId(id);
                customer.setName(name);
                customer.setDescription(description);

                customerController.save(customer);
                break;

            case 2:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                customerController.remove(customerController.getById(id));
                writeMessage("\n Done \n");
                break;

            case 3:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                writeMessage("\n Enter new customer name \n");
                name = readString();
                writeMessage("\n Enter new description \n");
                description = readString();

                customer.setId(id);
                customer.setName(name);
                customer.setDescription(description);

                customerController.update(customer);
                System.out.println("\n Done \n");
                break;

            case 4:
                writeMessage("\n Enter ID customer \n");
                id = readLong();
                customerController.getById(id);
                System.out.println(customerController.getById(id));
                System.out.println("\n Done \n");
                break;

            case 5:
                writeMessage("Customer base: \n");
                customerController.showAll();
                System.out.println("\n Done \n");
                break;

            case 6:
                writeMessage("Go to the main menu...");
                ConsoleHelper consoleHelper = new ConsoleHelper();
                consoleHelper.consoleHelp();
                break;
            default:
                break;
        }
        customerView();
    }

}
