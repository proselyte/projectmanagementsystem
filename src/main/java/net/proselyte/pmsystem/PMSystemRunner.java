package net.proselyte.pmsystem;

import net.proselyte.pmsystem.view.*;

import java.io.IOException;

/**
 * Main class of the project.
 *
 * @author Eugene Suleimanov
 */

public class PMSystemRunner {
    public static void main(String[] args) throws IOException{
        ConsoleHelper consoleHelper = new ConsoleHelper();
        consoleHelper.consoleHelp();
    }
}
