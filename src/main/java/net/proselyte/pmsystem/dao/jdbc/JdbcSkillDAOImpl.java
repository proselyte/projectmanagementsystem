package net.proselyte.pmsystem.dao.jdbc;

import net.proselyte.pmsystem.dao.SkillDAO;
import net.proselyte.pmsystem.model.Skill;
import net.proselyte.pmsystem.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static net.proselyte.pmsystem.util.ConnectionUtil.*;

/**
 * @author Created by Руслан on 18.12.2016.
 * @author Kyryl Potapenko
 */
public class JdbcSkillDAOImpl implements SkillDAO {

    public final static String INSERT = "INSERT INTO skills (ID, NAME) VALUES (?, ?)";
    public final static String GET_BY_ID = "SELECT * FROM skills WHERE ID = ?";
    public final static String UPDATE = "UPDATE skills SET  name = ? WHERE ID = ? ";
    public final static String DELETE = "DELETE FROM skills WHERE ID = ?";
    public final static String SHOWALL = "SELECT * FROM skills";
    private Skill skill;
    private List<Skill> skills = new ArrayList<>();

    public JdbcSkillDAOImpl() {
        try {
            getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Connection failed " + e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Driver not found " + e);
        }
    }

    @Override
    public Skill getById(Long id) {

        try {
            preparedStatement = connection.prepareStatement(GET_BY_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            skill = new Skill();
            if (resultSet.next()) {
                skill.setId(resultSet.getLong(1));
                skill.setName(resultSet.getString(2));
                return skill;
            } else {
                throw new SQLException("Cannot find Skill with id = " + id);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Exception occured while connetion to DB " + e);
        }
    }

    @Override
    public void save(Skill skill) {

        try {
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setLong(1, skill.getId());
            preparedStatement.setString(2, skill.getName());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Exception occured while connection to DB " + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void update(Skill skill) {

        try {
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, skill.getName());
            preparedStatement.setLong(2, skill.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        } finally {
            closePreparedStatement();
        }

    }

    @Override
    public void remove(Skill skill) {

        try {
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, skill.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Exception occurred while connecting to DB " + e);
        } finally {
            closePreparedStatement();
        }

    }

    public Collection<Skill> showAllBase() {
        try {
            try (Connection connetion = ConnectionUtil.connection) {
                try (Statement statement = connetion.createStatement()) {
                    Collection<Skill> list = new ArrayList<Skill>();
                    try (ResultSet resultSet = statement.executeQuery(SHOWALL)) {
                        while (resultSet.next()) {
                            Skill skill = new Skill();
                            skill.setId(resultSet.getLong(1));
                            skill.setName(resultSet.getString(2));
                            list.add(skill);
                        }
                    }
                    return list;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
