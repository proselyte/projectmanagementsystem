package net.proselyte.pmsystem.dao.hibernate;


import net.proselyte.pmsystem.dao.DocumentDAO;
import net.proselyte.pmsystem.model.Document;
import net.proselyte.pmsystem.model.Skill;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.sql.Insert;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by Iryna Seliutina on 04.02.2017.
 * Alena Eremina 19.02.17
 */
public class HibernateDocumentDAOImpl implements DocumentDAO {
    private SessionFactory sessionFactory;

    public HibernateDocumentDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Document> showAllBase() {
        // try (Session session = sessionFactory.openSession()){
        //   return (List<Document>) session.createQuery("FROM Document ").list();
        List<Document> documents = null;
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Document.class);
            documents = (List) criteria.list();
        }
        return documents;
    }

    @Override
    public Document getById(Long aLong) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Document.class, aLong);

        }
    }

    @Override
    public void save(Document document) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(document);
            session.getTransaction().commit();
        }

    }

    @Override
    public void update(Document document) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(document);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public void remove(Document document) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(document);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }
}