package net.proselyte.pmsystem.dao.hibernate;

import net.proselyte.pmsystem.dao.TeamDAO;
import net.proselyte.pmsystem.model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Руслан on 13.02.2017.
 */
public class HibernateTeamDAOImpl implements TeamDAO {

    public static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HibernateTeamDAOImpl.class);
    private SessionFactory sessionFactory;

    @Override
    public Team getById(Long aLong) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Team.class, aLong);
        }
    }

    @Override
    public void save(Team entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.save(entity);
                session.beginTransaction().commit();
            } catch (Exception e) {
                session.beginTransaction().rollback();
                LOGGER.error("Exception occurred while save new team, rollback");
            }
        }

    }

    @Override
    public void update(Team entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(entity);
                session.beginTransaction().commit();
            } catch (Exception e) {
                session.beginTransaction().rollback();
                LOGGER.error("Exception occurred while update team, rollback", e);
            }
        }

    }

    @Override
    public void remove(Team entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(entity);
                session.beginTransaction().commit();
            } catch (Exception e) {
                session.beginTransaction().rollback();
                LOGGER.error("Exception occurred while remove team, rollback", e);
            }
        }
    }

    public List<Team> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return (List<Team>) session.createQuery("From Team").list();
        }
    }
}
