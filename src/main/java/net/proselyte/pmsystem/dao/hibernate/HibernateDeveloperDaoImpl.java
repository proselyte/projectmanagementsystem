package net.proselyte.pmsystem.dao.hibernate;

import net.proselyte.pmsystem.dao.DeveloperDAO;
import net.proselyte.pmsystem.model.Developer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * Created by Raketa on 11.02.2017.
 */
public class HibernateDeveloperDaoImpl implements DeveloperDAO {
  private static final Logger LOGGER = LoggerFactory.getLogger(HibernateDeveloperDaoImpl.class);
  private SessionFactory sessionFactory;

  public HibernateDeveloperDaoImpl(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public Developer getById(Long id) {
    try (Session session = sessionFactory.getCurrentSession()) {
      return session.get(Developer.class, id);
    }
  }

  @Override
  public void save(Developer developer) {
    try (Session session = sessionFactory.openSession()) {
      session.save(developer);
    }
  }

  @Override
  public void update(Developer developer) {
    try (Session session = sessionFactory.getCurrentSession()) {
      try {
        session.beginTransaction();
        session.update(developer);
        session.getTransaction().commit();
      } catch (Exception e) {
        session.getTransaction().rollback();
        LOGGER.error("Exception occured while updating developer" + developer + ", rollback", e);
      }
    }
  }

  @Override
  public void remove(Developer developer) {
    try (Session session = sessionFactory.getCurrentSession()) {
      try {
        session.beginTransaction();
        session.delete(developer);
        session.getTransaction().commit();
      } catch (Exception e) {
        session.getTransaction().rollback();
        LOGGER.error("Exception occured while deleting developer" + developer + ", rollback", e);
      }
    }
  }

  @Override
  public void showAllDevelopers(Developer developer) {
    try (Session session = sessionFactory.getCurrentSession()) {
      List<Developer> developers = session.createQuery("FROM Developer").list();
      developers.forEach(System.out::println);
    }
  }
}
