package net.proselyte.pmsystem.dao.hibernate;

import net.proselyte.pmsystem.dao.SkillDAO;
import net.proselyte.pmsystem.model.Skill;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Alena on 02.02.2017.
 */
public class HibernateSkillDAOImpl implements SkillDAO {
    private SessionFactory sessionFactory;

    public HibernateSkillDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Skill> showAllBase() {
        List<Skill> skills = null;
        try (
                Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Skill.class);
            skills = (List) criteria.list();
        }
        return skills;
    }

    @Override
    public Skill getById(Long id) {
        Skill skill = null;
        try (Session session = sessionFactory.openSession()) {
            skill = session.get(Skill.class, id);
        }
        return skill;
    }

    @Override
    public void save(Skill skill) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.save(skill);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public void update(Skill skill) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(skill);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }

    }

    @Override
    public void remove(Skill skill) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(skill);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }
}
