package net.proselyte.pmsystem.dao.hibernate;

import net.proselyte.pmsystem.dao.CustomerDAO;
import net.proselyte.pmsystem.model.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.lang.annotation.Native;
import java.util.List;

/**
 * Hibernate realization of {@link CustomerDAO} interface.
 *
 * @author Kyryl Potapenko
 */
public class HibernateCustomerDAOImpl implements CustomerDAO {

    private SessionFactory sessionFactory;

    public HibernateCustomerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Customer getById(Long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Customer.class, id);
        }
    }

    @Override
    public void save(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(customer);
        }
    }

    @Override
    public void update(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(customer);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }

    @Override
    public void remove(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(customer);
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
    }

    public List<Customer> showAll() {
        List<Customer> customers = null;
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();

                customers = session.createQuery("FROM customers").list();
                session.getTransaction().commit();
            } catch (Exception e) {
                session.getTransaction().rollback();
            }
        }
        return customers;
    }
}
