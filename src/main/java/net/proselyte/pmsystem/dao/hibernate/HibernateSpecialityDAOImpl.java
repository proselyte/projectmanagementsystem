package net.proselyte.pmsystem.dao.hibernate;

import net.proselyte.pmsystem.dao.SpecialtyDAO;
import net.proselyte.pmsystem.model.Speciality;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Raketa on 22.01.2017.
 */
public class HibernateSpecialityDAOImpl implements SpecialtyDAO {

  private static final Logger LOGGER = LoggerFactory.getLogger(HibernateSpecialityDAOImpl.class);
  private SessionFactory sessionFactory;

  public HibernateSpecialityDAOImpl(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public List<Speciality> getAll() {
    try (Session session = sessionFactory.openSession()) {
      return (List<Speciality>) session.createQuery("FROM Speciality ").list();
    }
  }

  @Override
  public Speciality getById(Long id) {
    try (Session session = sessionFactory.openSession()) {
      return session.get(Speciality.class, id);
    }
  }

  @Override
  public void save(Speciality speciality) {
    try (Session session = sessionFactory.openSession()) {
      session.save(speciality);
    }
  }

  @Override
  public void update(Speciality speciality) {
    try (Session session = sessionFactory.openSession()) {
      try {
        session.beginTransaction();
        session.update(speciality);
        session.getTransaction().commit();
      } catch (Exception e) {
        session.getTransaction().rollback();
        LOGGER.error("Exception occurred while updating speciality"+speciality+", rollback", e);
      }
    }
  }

  @Override
  public void remove(Speciality speciality) {
    try (Session session = sessionFactory.openSession()) {
      try {
        session.beginTransaction();
        session.delete(speciality);
        session.getTransaction().commit();
      } catch (Exception e) {
        session.getTransaction().rollback();
        LOGGER.error("Exception occurred while updating speciality, rollback", e);
      }
    }
  }
}
