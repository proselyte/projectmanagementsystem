package net.proselyte.pmsystem.dao.hibernate;

import net.proselyte.pmsystem.dao.ProjectDAO;
import net.proselyte.pmsystem.model.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * <<<<<<< HEAD
 * Created by Alena on 19.02.2017.
 */
public class HibernateProjectDAOImpl implements ProjectDAO {

    private SessionFactory sessionFactory;

    public static final Logger LOGGER = LoggerFactory.getLogger(HibernateProjectDAOImpl.class);

    public HibernateProjectDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Project getById(Long aLong) {

        try (Session session = sessionFactory.openSession()) {
            LOGGER.info("Get project by ID", aLong);
            return session.get(Project.class, aLong);
        }
    }

    public void save(Project entity) {

        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.save(entity);
                session.getTransaction().commit();
                LOGGER.info("Save project successfully", entity);
            } catch (Exception e) {
                session.getTransaction().rollback();
                LOGGER.error("Exception occurred while save project" + entity + ", rollback", e);
            }
        }

    }

    public void update(Project entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.update(entity);
                session.getTransaction().commit();
                LOGGER.info("Update project successfully");
            } catch (Exception e) {
                session.getTransaction().rollback();
                LOGGER.error("Exception occurred while update project" + entity + ", rollback", e);
            }
        }

    }

    @Override
    public void remove(Project entity) {
        try (Session session = sessionFactory.openSession()) {
            try {
                session.beginTransaction();
                session.delete(entity);
                session.getTransaction().commit();
                LOGGER.info("Delete project successfully");
            } catch (Exception e) {
                session.getTransaction().rollback();
                LOGGER.error("Exception occurred while delete project" + entity + ", rollback", e);
            }
        }
    }

    public List<Project> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return (List<Project>) session.createQuery("from Project").list();
        }
    }
}
